import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class YandexSearchPage extends Base {
    protected YandexSearchPage(WebDriver driver) {
        this.driver = driver;
    }


    protected void search(String searchWord) {
        WebElement element = driver.findElement(By.id("text"));
        element.sendKeys(searchWord);
    }

    protected String getSearchText() {
        //WebElement explicitWait = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='suggest2__content suggest2__content_theme_normal']")));

        List<WebElement> list = driver.findElements(By.xpath("//div[@class='suggest2__content suggest2__content_theme_normal']/li"));
        return list.get(0).getText();
        //Беру первый элемент из списка, выданного поисковиком
    }

    protected YandexImagePage clickImage() {
        WebElement element = driver.findElement(By.linkText("Картинки"));
        element.click();
        return new YandexImagePage(driver);
        //Так как производится переход на другую страницу, то это будет новый объект
    }
}
