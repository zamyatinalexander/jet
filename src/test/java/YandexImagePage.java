import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YandexImagePage extends Base {
    private WebDriver driver;

    protected YandexImagePage(WebDriver driver) {
        this.driver = driver;
    }

    protected String getUrl() {
        return driver.getCurrentUrl();
    }

    protected String getMenu(){
        WebElement element = driver.findElement(By.xpath("//div[@class='collections-menu']/a[text()='Моя лента']"));
        return element.getText();
    }
}
