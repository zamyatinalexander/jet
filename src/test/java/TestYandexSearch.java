import org.testng.Assert;
import org.testng.annotations.Test;

public class TestYandexSearch extends Base {
    @Test
    public void testSearchWeather() {
        openSite("yandex.ru");
        YandexSearchPage page = new YandexSearchPage(driver);
        page.search("Погода");
        String result = page.getSearchText();
        System.out.println(result);
    }

    @Test
    public void testSearchLipeck() {
        openSite("yandex.ru");
        YandexSearchPage page = new YandexSearchPage(driver);
        page.search("Липецк");
        String result = page.getSearchText();
        System.out.println(result);
    }

    @Test
    public void testSearchLoto() {
        openSite("yandex.ru");
        YandexSearchPage page = new YandexSearchPage(driver);
        page.search("Лото");
        String result = page.getSearchText();
        System.out.println(result);
    }

    @Test
    public void testImageDisplay() {
        openSite("yandex.ru");
        YandexSearchPage page = new YandexSearchPage(driver);
        YandexImagePage imagePage = page.clickImage();
        Assert.assertTrue(imagePage.getUrl().contains("https://yandex.ru/images/"), "Не была открыта страница Картинки");
        Assert.assertTrue(imagePage.getMenu().contains("Моя лента"), "Не была открыта страница Картинки, не найден элемент 'Моя лента'");
    }

}
